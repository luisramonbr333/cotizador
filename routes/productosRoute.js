var express = require('express');
var router = express.Router();
const {obtenerProductos,agregarProducto, actualizarProducto, eliminarProducto} = require('../controllers/productosController')

/* GET obtener todos los productos */
router.get('/', obtenerProductos);

/* POST insertar producto. */
router.post('/', agregarProducto );

  /* PUT editar producto */
router.put('/:idProducto', actualizarProducto);

  /* DELETE eliminar producto  */
router.delete('/:idProducto',  eliminarProducto);

module.exports = router;