var express = require('express');
var router = express.Router();
const {obtenerPlazo, agregarPlazo, actualizarPlazo, eliminarPlazo} = require('../controllers/plazosController')

/* GET obtener todos los Plazo */
router.get('/', obtenerPlazo);

/* POST insertar Plazo. */
router.post('/', agregarPlazo );

  /* PUT editar Plazo */
router.put('/:idPlazos', actualizarPlazo);

  /* DELETE eliminar Plazo  */
router.delete('/:idPlazos',  eliminarPlazo);

module.exports = router;