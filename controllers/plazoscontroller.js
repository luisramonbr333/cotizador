const plazosModel = require('../models/plazosModel')

const obtenerPlazo = async(req, res, next)=>{
    const Plazos = await plazosModel.find()
     res.send(Plazos);
}

const agregarPlazo = async(req, res, next)=>{

    const Plazos = new plazosModel({
        ...req.body
    })
    await Plazos.save();
    res.status(201).send()
}

const actualizarPlazo = async(req, res, next)=>{
    await plazosModel.findByIdAndUpdate(req.params.idPlazos,{...req.body})
    res.status(200).send()
}

const eliminarPlazo = async(req, res, next)=>{
    await plazosModel.findByIdAndDelete(req.params.idPlazos)
    res.status(200).send()
}
module.exports = {obtenerPlazo, agregarPlazo, actualizarPlazo, eliminarPlazo}