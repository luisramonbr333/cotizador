const productosModel = require('../models/productosModel')

const obtenerProductos = async(req, res, next)=>{
    const productos = await productosModel.find()
     res.send(productos);
}

const agregarProducto = async(req, res, next)=>{

    const producto = new productosModel({
        ...req.body
    })
    await producto.save();
    res.status(201).send()
}

const actualizarProducto = async(req, res, next)=>{
    await productosModel.findByIdAndUpdate(req.params.idProducto,{...req.body})
    res.status(200).send()
}

const eliminarProducto = async(req, res, next)=>{
    await productosModel.findByIdAndDelete(req.params.idProducto)
    res.status(200).send()
}
module.exports = {obtenerProductos, agregarProducto, actualizarProducto, eliminarProducto}