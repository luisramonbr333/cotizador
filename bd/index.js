const mongoose = require('mongoose');

const dbConnection = async() => {
    try {
        await mongoose.connect(process.env.DB_CONNECTION);
        console.log('Conectado a la base de datos');
    } catch (error) {
        console.log(error);
        throw new Error('No se pudo iniciar la base de datos')
    }
}

module.exports = {
    dbConnection
}
