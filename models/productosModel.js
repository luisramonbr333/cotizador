const mongoose = require('mongoose');

const ProdctosSchema = mongoose.Schema({
    description: String,
    precio: Number,
    sku: Number,
    id: String
})

module.exports = mongoose.model('Productos', ProdctosSchema);
