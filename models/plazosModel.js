const mongoose = require('mongoose');

const PlazosSchema = mongoose.Schema({
    numeroPlazo : Number,
    tasaNormal: Number,
    tasaPuntual: Number
    
})

module.exports = mongoose.model('Plazos', PlazosSchema);
